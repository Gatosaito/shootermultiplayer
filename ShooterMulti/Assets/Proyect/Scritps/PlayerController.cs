using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    #region Variables

    [SerializeField] internal Camera cam;
    [SerializeField] private Transform pointOfView;
    [SerializeField] internal Transform gunPoint;
    [SerializeField] internal Transform recoil;
    [SerializeField] private CharacterController controller;

    private float horizontalRotationStore;
    private float verticalRotationStore;
    private float walkSpeed = 5;
    private float runSpeed = 10;
    public float jumpForce = 6f;
    private float gravityMod = 2.5f;

    [SerializeField] private float actualSpeed;
    [SerializeField] private Vector2 mouseInput;
    [SerializeField] private Vector3 direccion;
    [SerializeField] private Vector3 movement;

    [Header("Ground Detection")]
    [SerializeField] private bool isGrounded;
    [SerializeField] private float radio;
    [SerializeField] private float distance;
    [SerializeField] private Vector3 offset;
    [SerializeField] private LayerMask lm;

    #endregion

    #region Unity Functions

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;

        controller.GetComponent<CharacterController>();
        cam = Camera.main;
    }

    private void Update()
    {
        Rotate();
        Movement();

    }

    private void LateUpdate()
    {
        cam.transform.position = recoil.position;
        cam.transform.rotation = recoil.rotation;

        gunPoint.transform.position = recoil.position;
        gunPoint.transform.rotation = recoil.rotation;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position + offset, radio);

        if (Physics.SphereCast(transform.position + offset, radio, Vector3.down, out RaycastHit hit, distance, lm)) 
        {
            Gizmos.color = Color.green;
            Vector3 endpoint = ((transform.position + offset) + (Vector3.down * distance));
            Gizmos.DrawWireSphere(endpoint, radio);
            Gizmos.DrawLine(transform.position + offset, endpoint);

            Gizmos.DrawSphere(hit.point, 0.1f);
        }
        else
        {
            Gizmos.color = Color.red;
            Vector3 endpoint = ((transform.position + offset) + (Vector3.down * distance));
            Gizmos.DrawWireSphere(endpoint, radio);
            Gizmos.DrawLine(transform.position + offset, endpoint);
        }
    }

    #endregion

    #region Custom Functions

    private void Rotate() 
    {
        mouseInput = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

        horizontalRotationStore += mouseInput.x;
        verticalRotationStore -= mouseInput.y;

        verticalRotationStore = Mathf.Clamp(verticalRotationStore, -60f, 60f);

        transform.rotation = Quaternion.Euler(0f, horizontalRotationStore, 0f);
        pointOfView.transform.localRotation = Quaternion.Euler(verticalRotationStore, 0f, 0f);
    }

    private void Movement() 
    {
        direccion = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));
        
        float velY = movement.y;
        movement = ((transform.forward * direccion.z) + (transform.right * direccion.x)).normalized;
        movement.y = velY;


        if (Input.GetButton("Fire3"))
        {
            actualSpeed = runSpeed;
        }
        else
        {
            actualSpeed = walkSpeed;
        }

        if (IsGrounded())
        {
            movement.y = 0;
        }

        if (Input.GetButtonDown("Jump") && IsGrounded())
        {
            movement.y = jumpForce;
        }

        movement.y += Physics.gravity.y * Time.deltaTime * gravityMod;
        controller.Move (movement * (actualSpeed * Time.deltaTime));
    }

    private bool IsGrounded() 
    {
        isGrounded = false;

        if (Physics.SphereCast(transform.position + offset, radio, Vector3.down, out RaycastHit hit, distance, lm))
        {
            isGrounded = true;
        }

        return isGrounded;
    }

    #endregion
}
