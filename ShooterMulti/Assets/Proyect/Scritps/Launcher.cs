using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using TMPro;
using Photon.Realtime;

public class Launcher : MonoBehaviourPunCallbacks
{
    #region Variables
    public static Launcher Instance;

    [SerializeField] GameObject[] screenObjects;

    [SerializeField] TMP_InputField roomNameInput;

    [SerializeField] TMP_Text infoText;
    [SerializeField] TMP_Text roomName;
    [SerializeField] TMP_Text errorText;

    [SerializeField] RoomButtom prefBotonSala;
    [SerializeField] Transform posBotonSala;
    [SerializeField] List<RoomButtom> roomButtoms = new List<RoomButtom>();

    #endregion

    #region Unity Funtions
    private void Awake()
    {
        if (Instance == null)
        {

            Instance = this;

        }
        else
        {

            Destroy(this);

        }
    }
    private void Start()
    {
        infoText.text = "Connecting to Network...";
        SetScreenObjects(0);
        PhotonNetwork.ConnectUsingSettings();
    }
    #endregion

    #region Photon
    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
        SetScreenObjects(1);
    }

    public override void OnJoinedRoom()
    {
        roomName.text = PhotonNetwork.CurrentRoom.Name;
        SetScreenObjects(3);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        errorText.text = "se fallo en crear la sala \n" + message;
        SetScreenObjects(4);
    }

    public override void OnLeftRoom()
    {
        SetScreenObjects(1);
    }

    public void Back()
    {
        PhotonNetwork.LeaveRoom();
        SetScreenObjects(0);
        infoText.text = "Leave Room";
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        for (int i = 0; i < roomList.Count; i++)
        {
            if (roomList[i].RemovedFromList)
            {
                Debug.Log($"Room Name: {roomList[i].Name}");

                for (int j = 0; j < roomButtoms.Count; j++)
                {
                    if (roomList[i].Name == roomButtoms[j].info.Name)
                    {
                        GameObject go = roomButtoms[j].gameObject;
                        roomButtoms.Remove(roomButtoms[j]);
                        Destroy(go);
                    }
                }
            }
        }

        for (int i = 0; i < roomList.Count; i++)
        {
            if (roomList[i].PlayerCount != roomList[i].MaxPlayers && !roomList[i].RemovedFromList)
            {
                RoomButtom newRoomButtom = Instantiate(prefBotonSala, posBotonSala);
                newRoomButtom.SetButtonDetails(roomList[i]);

                Debug.Log(roomList);
                roomButtoms.Add(newRoomButtom);
            }
        }
    }

    #endregion

    #region Custom Funtions

    public void SetScreenObjects(int index) 
    {
        for (int i = 0; i < screenObjects.Length; i++)
        {
            screenObjects[i].SetActive(i == index);
        }   
    }

    public void CreateRoom() 
    {
        if (!string.IsNullOrEmpty(roomNameInput.text))
        {
            RoomOptions options = new RoomOptions();
            options.MaxPlayers = 8;

            infoText.text = "Creating Room...";
            
            PhotonNetwork.CreateRoom(roomNameInput.text);


            SetScreenObjects(0);
            
        }
    }

    public void JoinRoom(RoomInfo info)
    {
        PhotonNetwork.JoinRoom(info.Name);
        infoText.text = "Joining Room...";
        SetScreenObjects(0);
    }
    #endregion

}
