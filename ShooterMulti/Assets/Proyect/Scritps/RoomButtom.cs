using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Realtime;

public class RoomButtom : MonoBehaviour
{
    #region Variables
    [SerializeField] TMP_Text nombreDeLaSala;
    
    public RoomInfo info;


    #endregion

    #region Unity Functions

    #endregion

    #region Photon

    #endregion

    #region Custom Functions
    public void SetButtonDetails(RoomInfo inputInfo) 
    {
        info = inputInfo;

        nombreDeLaSala.text = info.Name;
    }

    public void OnJoiniRoom() 
    {
        Launcher.Instance.JoinRoom(info);
    }

    #endregion
}
