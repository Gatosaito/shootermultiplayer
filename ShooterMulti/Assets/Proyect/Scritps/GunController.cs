using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class GunController : MonoBehaviour
{
    #region Variables

    public PlayerController playerController;
    public GameObject preBulletHole;

    public Gun[] guns;
    public Gun actualGun;
    public int indexGun = 0;
    public int maxGun = 3;

    float lastShootTime = 0;
    Vector3 currentRotation;
    Vector3 targetRotation;
    public float returnSpeed;
    public float snappines;

    float lastReload;
    bool reloading;

    bool isChaning;
    float changeTime;
    float lastChangeTime;

    public float distance;

    public AudioClip recarga;

    public VisualEffect balas;

    #endregion

    #region Unity Functions

    private void Update()
    {
        if (actualGun != null)
        {
            if (lastShootTime <= 0)
            {
                if (!actualGun.data.automatic)
                {
                    if (Input.GetButtonDown("Fire1"))
                    {
                        if (actualGun.data.actualAmmo > 0)
                        {
                            Shoot();
                        }
                    }
                }
                else
                {
                    if (Input.GetButton("Fire1"))
                    {
                        if (actualGun.data.actualAmmo > 0)
                        {
                            Shoot();
                        }
                    }
                }
            }
            if (Input.GetButtonDown("Reload")&& !reloading)
            {
                if (actualGun.data.actualAmmo < actualGun.data.maxAmmoCount)
                {
                    lastReload = 0;
                    AudioSource.PlayClipAtPoint(recarga, gameObject.transform.position);
                    reloading = true;
                }
            }

            if (Input.GetButtonDown("Drop") && !reloading && !isChaning) 
            {
                Drop();
            }
        }
        else
        {
            if (actualGun == null)
            {

            }
        }

        if (lastShootTime >= 0)
        {
            lastShootTime -= Time.deltaTime;
        }
        if (reloading)
        {
            lastReload += Time.deltaTime;
            if (lastReload >= actualGun.data.reloadTime)
            {
                reloading = false;
                Reload();
            }
        }

        targetRotation = Vector3.Lerp(targetRotation, Vector3.zero, returnSpeed * Time.deltaTime);
        currentRotation = Vector3.Slerp(currentRotation, targetRotation, snappines * Time.deltaTime);
        playerController.recoil.localRotation = Quaternion.Euler(currentRotation);

        if (Input.GetButtonDown("Gun1"))
        {
            if (indexGun != 0)
            {
                indexGun = 0;
                lastChangeTime = 0;

                if (actualGun != null)
                {
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChaning = true;
            }
        }
        if (Input.GetButtonDown("Gun2"))
        {
            if (indexGun != 1)
            {
                indexGun = 1;
                lastChangeTime = 0;

                if (actualGun != null)
                {
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChaning = true;
            }
        }
        if (Input.GetButtonDown("Gun3"))
        {
            if (indexGun != 2)
            {
                indexGun = 2;
                lastChangeTime = 0;

                if (actualGun != null)
                {
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChaning = true;
            }
        }

        if (isChaning)
        {
            lastChangeTime += Time.deltaTime;
            if (lastChangeTime >= changeTime)
            {
                isChaning = false;
                ChangeGun(indexGun);
            }
        }

        if (Physics.Raycast(playerController.cam.transform.position, playerController.cam.transform.forward, out RaycastHit hit, distance))
        {
            if (hit.transform.tag == ("IsGun"))
            {
                if (Input.GetButtonDown("Drop") && !reloading && !isChaning)
                {
                    GetGun(hit.transform.GetComponent<Gun>());
                }
            }
        }
    }

    #endregion

    #region Custom Functions

    private void OnDrawGizmos()
    {
        Debug.DrawRay(playerController.cam.transform.position, playerController.cam.transform.forward * distance);
    }

    private void Shoot() 
    {
        if (reloading == false)
        {
            if (Physics.Raycast(playerController.cam.transform.position, playerController.cam.transform.forward, out RaycastHit hit, actualGun.data.range))
            {
                if (hit.transform != null)
                {
                    Debug.Log($"we shooting at: {hit.transform.name}");
                    GameObject preb =  Instantiate(preBulletHole, hit.point + hit.normal * 0.001f, Quaternion.LookRotation(hit.normal, Vector3.up));
                    Destroy(preb, 5f);
                }
            }

            actualGun.data.actualAmmo--;
            lastShootTime = actualGun.data.fireRate;
            actualGun.donde.PlayOneShot(actualGun.que);
            AddRecoil();
            GameObject gp = VisualEffect.Instantiate(balas, actualGun.pos.position, actualGun.pos.rotation).gameObject;
            gp.transform.parent = actualGun.pos;
            Destroy(gp, 2f);
        }
    }

    void AddRecoil() 
    {
        currentRotation -= new Vector3(actualGun.data.recoil.x, Random.Range(-actualGun.data.recoil.y, actualGun.data.recoil.y), 0f);
    }

    void Reload() 
    {
        actualGun.data.actualAmmo = actualGun.data.maxAmmoCount;
    }

    void ChangeGun(int index) 
    {
        if (guns[index] != null)
        {
            actualGun = guns[index];
            actualGun.gameObject.SetActive(true);
        }
    }

    void Drop() 
    {
        actualGun.GetComponent<Rigidbody>().useGravity = true;
        actualGun.GetComponent<Rigidbody>().isKinematic = false;

        actualGun.gameObject.transform.SetParent(null);
        actualGun = null;

        guns[indexGun] = null;
    }

    void GetGun(Gun getGun) 
    {
        if (actualGun != null)
        {
            Drop();
        }

        actualGun = getGun;

        actualGun.GetComponent<Rigidbody>().useGravity = false;
        actualGun.GetComponent<Rigidbody>().isKinematic = true;

        actualGun.transform.parent = playerController.gunPoint;
        actualGun.transform.localPosition = actualGun.data.offset;
        actualGun.transform.localRotation = Quaternion.identity;

        guns[indexGun] = actualGun;
    }
    #endregion
}
