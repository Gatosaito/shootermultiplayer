using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    #region Variables

    public GunData data;

    public AudioSource donde;
    public AudioClip que;

    public Transform pos;

    #endregion

    #region Unity Functions

    private void Awake()
    {
        data.actualAmmo = data.maxAmmoCount;


    }

    #endregion
}
