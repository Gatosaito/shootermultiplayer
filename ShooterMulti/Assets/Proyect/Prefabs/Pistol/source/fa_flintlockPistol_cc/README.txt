**Flintlock Pistol, 18TH. C. with Grey Ornate Handle**

*Reproduction of flintlock gun made of metal and wood with simulated mechanism of loading and firing.
 Historical DENIX reproduction weapons XVI-XIX C.*

*36.2 x 5.7 x 14.2 cm (135 micrometers per texel @ 2k)*
*Scanned in 2019 using older tech*

Scanned using advanced technology developed by inciprocal Inc. that enables highly photo-realistic reproduction of real-world products in virtual environments. Our hardware and software technology combines advanced photometry, structured light, photogrammtery and light fields to capture and generate accurate material representations from tens of thousands of images targeting real-time and offline path-traced PBR compatible renderers.

Zip file includes low-poly OBJ mesh (in meters) with a set of 2k PBR textures compressed with *lossless* JPEG (no chroma sub-sampling).

Copyright (C) 2019-2021 inciprocal Inc.
This work is licensed under a Creative Commons Attribution 4.0 International (CC BY 4.0) License.
